/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package java.ui;

import androidx.annotation.NonNull;

import javax.inject.Inject;

public class SimpleFragmentPresenter implements SimpleFragmentContract.Presenter {

    private SimpleFragmentContract.View view;

    @Inject
    public SimpleFragmentPresenter(@NonNull SimpleFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void onLayoutClick() {
        view.showText("layout text hint -java");
    }
}
